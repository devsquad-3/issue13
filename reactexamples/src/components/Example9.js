import React, { Component } from 'react';
import data from "./data"; 

const socialMediaList = data.SocialMedias;


class MediaList extends Component {
	render() {
		return (
            <ul>
                {socialMediaList.map(media => (<li>{media}</li>))}
            </ul>
        );
    }
} 

class Experience extends Component {
	render() {
		return (
            <div>
                {
					data.Experiences.map((experience, i) => {
						return (
							<div key={i}>
								<div>
									<a href={experience.url}>
										
									</a>
									<div>
										<div>
											<a href={experience.url}>{experience.companyName}</a>
										</div>
											{experience.roles.map(function (role, i) { 
												return <div key={i}>
													<h5>{role.title}</h5>
													<span>{role.startDate}</span>
													<span>{role.location}</span>
													<p>{role.description}</p>
												</div>
											})}
									</div>
								</div>
							</div>
						);
					})
				}
            </div>
        );
    }
} 


class Skill extends Component {
	render() {
		return (
            <div>
                {
                  data.Skills.map((skill) => {
                    return (
                      <div>
                        <h4>{skill.Area}</h4>
                        <ul>
                          {
                            skill.SkillSet.map((skillDetail) => {
                              return (
                                  <li>
                                    {skillDetail.Name}
                                  </li>
                              );
                            })
                          }
                        </ul>
                      </div>
                    );
                  })
                } 
            </div>
        );
    }
} 

   
class GetData5 extends Component {
    render() {
        return (
            <div>
				    <MediaList></MediaList>
                    <Experience></Experience>
                    <Skill></Skill>
            </div>
        );
    }
}



export default function Example9 (){
    return(
        <div>
            <GetData5></GetData5>
        </div>
    )
}