
import { useEffect, useState } from "react";
import axios from 'axios'
import React from 'react';

// axios

function GetData2(){
    var [posts, setPosts] = useState([])

    useEffect(() =>{
        axios.get('https://jsonplaceholder.typicode.com/posts')
        .then(res => {
            console.log(res) // res meaning response
        })
        .catch(err => {
            console.log(err)
        })
    })

    return(

        <div>

            <ul>
                {
                    posts.map(post => <li key ={post.id} > {post.tile} </li>)
                }
            </ul>

        </div>

    )
}



export default function Example7 (){

    return(

        <div>
            <GetData2></GetData2>
        </div>


    )

}