import React from 'react';


//functional component
function DataLogic1(){
    var name1 = "variable can be re-assigned";
    const name2 = "var wont be reassigned"
    return(
        <div>
            <h1>{name1}</h1>
            <h2>{name2}</h2>
        </div>
    )


}

//class comopnent
var DataLogic2 = ()=>{

    function sayHello(){
        alert('Hello world');
    }

    return(
        <button onClick={sayHello}>Click Here!</button>
    )
}

function Example1(){
    return(
        <div>
            <DataLogic1></DataLogic1>
            <DataLogic2></DataLogic2>
        </div>
    )
}

export default Example1;