import React from 'react';

class UserData extends React.Component{

    constructor(props){

        super(props);
        this.state = {value: ' '};
    }

    handleChange = event => {
        this.setState({value : event.target.value});
    }

    handleSubmit(event){
        
        var data = document.getElementById("username").value;
        console.log('Hello' + data);
        document.getElementById("output").innerHTML = data;
        event.preventDefault();
    }

    render()
    {
        return(
            <form onSubmit ={this.handleSubmit}>
                User Name:
                <input type= "text" id="username"/>
                <input type="submit" value="Submit"/>
                <br></br><span id="output"></span>

            </form>

        );
    }


}

function Example5()
{
    return(
        <div>
            <UserData></UserData>
        </div>

    );
}

export default Example5;