import React from 'react';
import ReactDOM from 'react-dom';

class JSXExample1 extends React.Component{

    render(){
        return <h2>JSX is getting rendered</h2>
    }
}

function JSXExample2(user){

    return user.fname + ' ' + user.lname;
}

var data1 = {
    fname : 'ABC',
    lname : 'XYZ'
}

var element = (
    <h2> hello, {JSXExample2(data1)}</h2>
);

ReactDOM.render(

    element, document.getElementById('root')
)

function Example4(){
    return(

        <div>
            <JSXExample1></JSXExample1>
            
        </div>
    )
}

export default Example4;