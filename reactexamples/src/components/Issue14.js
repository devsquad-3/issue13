
import { useEffect, useState } from "react";
import axios from 'axios'
import React from 'react';

// axios

function GetData2(){
    var [posts, setPosts] = useState([])

    useEffect(() =>{
        axios.get('https://jsonplaceholder.typicode.com/posts')
        .then(res => {
            console.log(res) // res meaning response
            setPosts(res.data);

        })
        .catch(err => {
            console.log(err)
        })
    })

    return(

        <div>

            <ul>
                {
                    posts.map(post => <li key ={post.id} > {post.title} </li>)
                }
            </ul>

        </div>

    )
}



export default function Issue14 (){

    return(

        <div>
            <GetData2></GetData2>
        </div>


    )

}