import React from 'react';
// React JS 16.8 version onwards we have Hooks concept

import { useEffect, useState } from "react";

// useEffect and useState

// re-usable component
function GetData1(){
    
    var [count, setCount] = useState(0)

    useEffect(() =>{
        document.title = {count}
    })

    return(
        <div>

            <p> You have clicked {count} times</p>
            <button onClick = {() => setCount (count+1)} > Click Here</button>
        </div>
    )

}



export default function Example6 (){

    return(

        <div>
            <GetData1></GetData1>
        </div>

    )

}
