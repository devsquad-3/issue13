import React from 'react'

class Cart extends React.Component{


    constructor(props){
        super(props);
        this.state = {value: ' '};
    }

    handleSubmit(event){

        var productID = document.getElementById("id").value;
        document.getElementById("productID").innerHTML = productID;

        var productName = document.getElementById("name").value;
        document.getElementById("productName").innerHTML = productName;

        var productCat = document.getElementById("cat").value;
        document.getElementById("productCat").innerHTML = productCat;

        var unitPrice = document.getElementById("price").value;
        document.getElementById("unitprice").innerHTML = unitPrice;

        var quantity = document.getElementById("quantity").value;
        document.getElementById("productQuantity").innerHTML = quantity;

        var totalPrice = unitPrice * quantity;
        document.getElementById("total").innerHTML = totalPrice;

        event.preventDefault();
    }

    render()
    {
        return(
            <form onSubmit ={this.handleSubmit}>
                Product ID:
                <input type= "text" id="id"/><br></br>
                Product Name:
                <input type="text" id="name"/><br></br>
                Product Catergory:
                <input type="text" id="cat"/><br></br>
                Unit Price:
                <input type="text" id="price"/><br></br>
                Quantity:
                <input type="text" id="quantity"/><br></br>

                <input type="submit" value="Submit"/>
                <br></br>

                <br></br> Product ID: <span id="productID"></span><br></br>
                Product Name: <span id="productName"></span><br></br>
                Product Catergory: <span id="productCat"></span><br></br>
                Unit Price: <span id="unitprice"></span><br></br>
                Quantity: <span id="productQuantity"></span><br></br>

                Total Price: <span id="total"></span><br></br>

            </form>

        );
    }



}

function Issue13(){

    return(

        <div>
            <Cart></Cart>
        </div>

    );
}
export default Issue13;