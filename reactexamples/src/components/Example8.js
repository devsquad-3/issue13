import React from 'react';
import { JsonToTable } from "react-json-to-table";
// npm install react-json-to-table

// Approach - 1

function GetData4(){

    var employeeJSON = { 
        "name": "Tom", 
        "street": "Orchard", 
        "phone": "555 5555",
        "email": "tom@google.com" 
    };
  
    //convert json string to json object using JSON.parse function
    var jsonObject = JSON.parse(JSON.stringify(employeeJSON)); // use JSON.stringify to convert it to json string

    return(
        <div>
            <JsonToTable json={employeeJSON} />
        </div>
    );

}

export default function Example8 (){
    return(
        <div>
            <GetData4></GetData4>
        </div>
    )
}